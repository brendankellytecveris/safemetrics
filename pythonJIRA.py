
from jira import JIRA
import sqlite3, requests, json, logging, datetime, configparser, sys, openpyxl


def intDatabase():
	sqlite_file = 'PyJIRA.db'    # name of the sqlite database file
	conn = sqlite3.connect(sqlite_file)
	return conn

def initDatabasePG(settings):
	connectionString = 'dbname=\''+settings.get('dbConfigOne', 'dbname')+'\' host=\''+settings.get('dbConfigOne', 'host')+'\' user=\'' + settings.get('dbConfigOne', 'user')+'\' password=\''+ settings.get('dbConfigOne', 'password')+ '\''
	#establish connection
	try:
		conn = psycopg2.connect(connectionString)
	except:
		log.info('no puedo hablar con el base de datos')
	cur = conn.cursor()

def pullDatabase(sql):
	sqlite_file = 'PyJIRA.db'    # name of the sqlite database file
	conn = sqlite3.connect(sqlite_file)
	c = conn.cursor()
	c = c.execute(sql)
	results = c.fetchall()
	conn.close()
	return results

def writeDatabaseOnce(sql,values):
	sqlite_file = 'PyJIRA.db'    # name of the sqlite database file
	conn = sqlite3.connect(sqlite_file)
	c = conn.cursor()
	c.execute(sql,values)
	conn.commit()
	conn.close()
	return 

def writeDatabaseMany(sql,values):
	sqlite_file = 'PyJIRA.db'    # name of the sqlite database file
	conn = sqlite3.connect(sqlite_file)
	c = conn.cursor()
	c.executemany(sql,values)
	conn.commit()
	conn.close()
	return 

def jiraAuth(settings):
	jira = JIRA(basic_auth=(settings.get('apiSettings', 'username'), settings.get('apiSettings', 'password')), options={'server': settings.get('apiSettings', 'baseURL')})
	return jira

def cleanSprint(issue):
	if len(issue.fields.customfield_10015) > 0:
		sprintText = issue.fields.customfield_10015[0]
		sprintName = sprintText[(sprintText.index('name=')+len('name=')):sprintText.index(',goal=')]
	else:
		sprintName=""
	return sprintName

def cleanFixVersion(issue):
	if len(issue.fields.fixVersions) > 0:
		fixVersion = issue.fields.fixVersions[0].name
	else: 
		fixVersion = ""
	return fixVersion

def cleanThemes(issue):
	if issue.fields.customfield_10808 == None:
		theme = ""
	elif len(issue.fields.customfield_10808)>0:
		theme = ""
		for i in issue.fields.customfield_10808:
			theme = theme + str(i.value) + ', '	
	return theme

def cleanResolution(issue):
	if issue.fields.resolution is not None:
		resolution = str(issue.fields.resolution)
	else:
		resolution =" "
	return resolution

def findProgEpicParent(issue):
	if len(issue.fields.issuelinks)>0:
		for link in issue.fields.issuelinks:
			if str(link.type) == "Parent":
				if hasattr(link, "inwardIssue"):
					parentkey = link.inwardIssue.key
			else:
				parentkey = ""
	else:
		parentkey = ""

	return parentkey


def popStories(jira):
	#pull all stories and enabler stories for open releases
	writeDatabaseOnce("drop table StoryTable","")
	writeDatabaseOnce("CREATE TABLE `StoryTable` ( `Key` TEXT, `Summary` TEXT, `StoryPoints` TEXT, `BusinessValue` TEXT, `Issue Type` TEXT, `Sprint` TEXT, `Resolution` TEXT, `Resolution Date` TEXT, `Release` TEXT, `ParentKey` TEXT  )","")

	stories = jira.search_issues('category= team AND issuetype in (Story, "Enabler Story", Maintenance)', maxResults=200)
	writeList = []
	for issue in stories:

		sprintName = cleanSprint(issue)
		fixVersion = cleanFixVersion(issue)
		resolution = cleanResolution(issue)

		issueData =[issue.key, issue.fields.summary, issue.fields.customfield_10021, issue.fields.customfield_10811, issue.fields.issuetype.name, sprintName, resolution, issue.fields.resolutiondate, fixVersion, issue.fields.customfield_10016]
		
		for i in range(len(issueData)):
			if not issueData[i]:
				issueData[i]= ""
		writeList.append(issueData)
		writeDatabaseOnce("INSERT INTO StoryTable VALUES (?,?,?,?,?,?,?,?,?,?)",issueData)

def popPortfolioEpics(jira):
	#pull all stories and enabler stories for open releases
	writeDatabaseOnce("drop table PortfolioEpicTable","")
	writeDatabaseOnce("CREATE TABLE `PortfolioEpicTable` ( `Key` TEXT, `Summary` TEXT, `Themes` TEXT, `JobSize` TEXT, `BV` TEXT, `Created` TEXT, `ResolutionDate` TEXT, `Release` TEXT )","")
	portEpics = jira.search_issues('category= portfolio AND issuetype = "Portfolio Epic" AND resolution is empty', maxResults=200)
	writeList = []
	for issue in portEpics:
		theme = cleanThemes(issue)
		sprintName = cleanSprint(issue)
		fixVersion = cleanFixVersion(issue)
		issueData =[issue.key, issue.fields.summary, theme, issue.fields.customfield_10804, sprintName, issue.fields.resolution, issue.fields.resolutiondate, fixVersion]
		#change the empty values into blank strings to write into the database
		for i in range(len(issueData)):
			if not issueData[i]:
				issueData[i]= ""
		writeList.append(issueData)
		writeDatabaseOnce("INSERT INTO PortfolioEpicTable VALUES (?,?,?,?,?,?,?,?)",issueData)

def popProgramEpics(jira):
	writeDatabaseOnce("drop table ProgramEpicTable","")
	writeDatabaseOnce("CREATE TABLE `ProgramEpicTable` ( `Key` TEXT, `Summary` TEXT, `ParentKey` TEXT, `BV` TEXT, `JobSize` TEXT, `Resolution` TEXT, `ResolutionDate` TEXT, `FixVersion` TEXT )","")

	#pull all stories and enabler stories for open releases
	progEpics = jira.search_issues('category= program AND issuetype = Epic AND resolution is empty', maxResults=200)
	writeList = []
	for issue in progEpics:
		sprintName = cleanSprint(issue)
		fixVersion = cleanFixVersion(issue)
		parentkey = findProgEpicParent(issue)
		issueData =[issue.key, issue.fields.summary, parentkey, issue.fields.customfield_10800, issue.fields.customfield_10804, issue.fields.resolution, issue.fields.resolutiondate, fixVersion]
		writeDatabaseOnce("INSERT INTO programEpicTable VALUES (?,?,?,?,?,?,?,?)",issueData)
	return progEpics

def enablerRatioReport(jira):
	today = datetime.date.today().strftime('%m-%d-%Y')
	enablers = jira.search_issues('project = RUN AND issuetype = Epic AND "Epic Type" = "Enabler Epic"', maxResults=20)
	features = jira.search_issues('project = RUN AND issuetype = Epic AND "Epic Type" = "Feature Epic"', maxResults=20)
	numEnablers = len(enablers)
	numFeatures = len(features)

	enablerJS = 0
	for i in enablers:
		enablerJS = enablerJS + i.fields.customfield_10804

	featureJS = 0
	for i in features:
		featureJS = featureJS + i.fields.customfield_10804
	ratioReport = [today,numEnablers, numFeatures, featureJS, enablerJS]
	return ratioReport

def spitOutReports():
	data = pullDatabase('select * from progEpicBurnup;')
	wb = openpyxl.load_workbook(filename = 'Output.xlsx')
	ws = wb['EpicBurnup']
	for row in data:
		ws.append(row)
	wb.save('Output.xlsx')

def progEpicBurnup(progEpics):
	#CREATE TABLE "progEpicBurnup" ( `Date` TEXT, `Epic Key` TEXT, `Epic Name` TEXT, `Total Count of Stories` TEXT, `Total Story Points of Stories` TEXT, `Count Completed Stories` TEXT, `Sum of Completed Story Points` TEXT )
	for epic in progEpics:
		epicData = {}
		#epicData['date'] = datetime.datetime.today().strftime("%m/%d/%Y")
		epicData['date'] = "4/16/2018"
		epicData['key'] = epic.key
		epicData['Epic Name'] = epic.fields.summary
		epicData['Parent key'] = findProgEpicParent(epic)
		epicData['Fix Version'] = cleanFixVersion(epic)
		count = pullDatabase("select count(*) from StoryTable where ParentKey = '%s'" % epic)
		epicData['Count Stories'] = count[0][0]
		sumsp = pullDatabase("select sum(StoryPoints) from StoryTable where ParentKey = '%s'" % epic)
		epicData['Sum Story Points'] = sumsp[0][0]
		countcomp = pullDatabase(""" select count(*) from StoryTable where ParentKey = '%s' and "Resolution Date" != "" """ % epic)
		epicData['Count Stories Complete'] = countcomp[0][0]
		sumspcomp = pullDatabase(""" select sum(StoryPoints) from StoryTable where ParentKey = '%s' and "Resolution Date" != "" """ % epic)
		epicData['Sum Story Points Complete'] = sumspcomp[0][0]
		writeDatabaseOnce("INSERT INTO progEpicBurnup VALUES (?,?,?,?,?,?,?,?,?)", list(epicData.values()))
	return


if __name__ == "__main__":
	settings = configparser.ConfigParser()
	settings._interpolation = configparser.ExtendedInterpolation()
	settings.read('config.ini')
	jira = jiraAuth(settings)
	conn = intDatabase()

	#check for python version 3
	if sys.version_info[0] < 3:
		raise Exception("Python 3 or a more recent version is required.")

	progEpics = popProgramEpics(jira)
	popStories(jira)
	popPortfolioEpics(jira)
	progEpicBurnup(progEpics)
	#export the data to excel 
	#spitOutReports()



'''List of Custom Fields:
Format issue.fields.customfield_10800
BV = customFieldId=10800
Business Value = customFieldId=10811
Job Size = customFieldId=10804
Sprint = customFieldId=10015
Story points = customFieldId=10021
WSJF = customFieldId=10805
Theme = 10808
'''
